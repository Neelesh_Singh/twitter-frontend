export const state = () => ({
  authenticated: false,
  myFollowing: [],
  user: {},
  tweets: {},
  userDetails: {},
})

export const mutations = {
  toggleAuthenticated(state, payload) {
    state.authenticated = !state.authenticated
    state.user = payload
  },
  updateTweets(state, payload) {
    state.tweets = payload
  },
  updateUserDetails(state, payload) {
    state.userDetails = payload
  },
  updateMyFollowing(state, payload) {
    state.myFollowing = payload
  },
  follow(state, payload) {
    state.userDetails.followers.push({
      ...payload,
    })
  },
}
